#!/usr/bin/env python
import sys
import random
from random import choice
from string import ascii_uppercase

#generate a random name for each of the profiles
class lib:
  def __init__(self):
    unknown = "unknown"  

  def _generate_random(self, extension):
    rand = ''.join(choice(ascii_uppercase) for i in range(12))
    rand_name = rand+extension
    return rand_name

if __name__=='__main__':
  lib = lib()
  lib._generate_random('.png')
