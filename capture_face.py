#!/usr/bin/env python
import cv2
import sys, time

class capture_face(object):
  def __init__(self, video_capture, dir_file):
    self.camera = video_capture
    self.ramp_frames = 30
    self.dir_file= dir_file
    self.get_capture()

  def get_image(self):
    retval, im = self.camera.read()
    return im
  
  def get_capture(self):
    for i in range(self.ramp_frames):
      temp = self.get_image()
    camera_capture = self.get_image()
    cv2.imwrite(self.dir_file, camera_capture)

if __name__=='__main__':
  video_capture = cv2.VideoCapture(0)
  dir_file = "/root/Desktop/ci20FaceRecognition/ion.png"
  capture_face = capture_face(video_capture, dir_file)#file, camera

