#!/usr/bin/env python
import MySQLdb

db = MySQLdb.connect(host="localhost",
                     user="root",
                     passwd="",
                     db="users_details")
cur = db.cursor()
#sql = """INSERT INTO users(name, image, date) 
#         VALUES ('ion', 'image', 23/05/2016)"""

sql = """SELECT * FROM users"""

cur.execute(sql)
results = cur.fetchall()
for row in results: 
  name = row[0]
  image = row[1]
  date = row[2]
  print(name, image, date)

db.commit()

db.close()
